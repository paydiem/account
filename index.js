const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('express-jwt');
const secret = '#dadliCAN';

const app = express();
const API_PORT = process.env.API_PORT || 9090;
const jwtValidate = jwt({ secret }).unless({path: ['/account/summary']});

app.use(bodyParser.json());

app.get('/account', (req, res) => {
    res
    .status(200)
    .send(`Your account is public.`);
});

app.get('/account/authenticate', jwtValidate , (req, res) => {
    res
    .status(200)
    .send(`Your account is authenticated. You account summary is listed below....`);
});

app.get('*', (req,res) => {
    res.sendStatus(404);
});

app.listen(API_PORT, () => {
    console.log(`API listening on port ${API_PORT}`);
});